# Easy Load SSH

A simple load balancer for SSH connections, using `pdsh`

## Usage

### `elssh run`
At its simplest, the command  will fetch your username from your computer and connect to the least busy computer in the linux portion of the CH215 lab on the Mines campus.

### `elssh run -r bb136-[01-24]`
Run `elssh` with the specified range of hosts (`pdsh` syntax), and offer to save it in your config under a name of your choosing.

### `elssh run podiums`
Run `elssh` with the list of host range patterns specified in the `hostgroups > podiums` path in your `~/.elsshrc`. For example:

```
hostgroups:
  podiums:
    - pod1
    - pod2
```
